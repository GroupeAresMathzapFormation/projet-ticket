<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Etat;
use App\Entity\Membre;


class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index()
    {
        return $this->render('security/login.html.twig');
    }
	/**
     * @Route("/connexion", name="connexion")
     */
    public function connexion(Request $request, EntityManagerInterface $manager)
    {
			
        //on récupère les id et pwd de la page login
		$email = $request->request->get("email");
		$password = $request->request->get("password");
		//on selectionne dans la base de données
		$aUser = $manager->getRepository(Membre::class)->findBy(["email"=>$email, "password"=>$password]);
		//on teste le résultat
		if (sizeof($aUser)==1) {
			$utilisateur = new Membre();
            $utilisateur = $aUser[0];
			//on démarre la session
			$sess = $request->getSession();
			//on met à jour les variables de session
			$sess->set("idUtilisateur", $utilisateur->getId());
            $sess->set("nomUtilisateur", $utilisateur->getNom());
            $sess->set("prenomUtilisateur", $utilisateur->getPrenom());
            $sess->set("roleUtilisateur", $utilisateur->getRole());
			return $this->redirectToRoute('liste_etat');
		}
		return $this->render('security/login.html.twig');
    }
	/**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request, EntityManagerInterface $manager)
    {
		$sess = $request->getSession();
		$sess->invalidate();
        $sess->clear();
        return $this->redirect("/login");
	}
	
}
