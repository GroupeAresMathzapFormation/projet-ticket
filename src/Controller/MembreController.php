<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Etat;
use App\Entity\Membre;

class MembreController extends AbstractController
{
    /**
     * @Route("/createMembre", name="create_membre")
     */
    public function createMembre()
    {
        return $this->render('membre/createMembre.html.twig');
    }
	/**
     * @Route("/insertMembre", name="insert_membre")
     */
    public function insertMembre(Request $request, EntityManagerInterface $manager)
    {
		$membre = new Membre();
		$membre->setNom($request->request->get("nomMembre"));
		$membre->setPrenom($request->request->get("prenomMembre"));
		$membre->setEmail($request->request->get("emailMembre"));
		$membre->setPassword($request->request->get("passwordMembre"));
		$membre->setRole(intval($request->request->get("roleMembre")));
		$manager->persist($membre);
        $manager->flush();       
        return new response('true');
    }
}
