<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* membre/createMembre.html.twig */
class __TwigTemplate_206aa1d491bf65204c207c431e9d9decbe01661e370474f8b73abc64f927fe30 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "membre/createMembre.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "membre/createMembre.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Créer un membre";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"example-wrapper\">
    <h1>Créer un Membre</h1>
</div>
<form action=\"/insertMembre\" method=\"post\">
  <fieldset>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Nom du nouveau membre</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"nomMembre\" placeholder=\"Entrer un nom\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Prénom du nouveau membre</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"prenomMembre\" placeholder=\"Entrer un prenom\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Email du nouveau membre</label>
      <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"emailMembre\" placeholder=\"Entrer un email\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Password du nouveau membre</label>
      <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"passwordMembre\" placeholder=\"Entrer un email\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Rôle du nouveau membre</label>
      <select class=\"form-control\" name=\"roleMembre\">
\t\t\t<option  class=\"form-control\" value=\"3\"> Rédacteur </option>
\t\t\t<option  class=\"form-control\" value=\"2\"> Technicien </option>
\t\t\t<option  class=\"form-control\" value=\"1\"> Administrateur </option>
\t  </select>
    </div>
  </fieldset>
   <button type=\"submit\" class=\"btn btn-primary\">Créer</button>
   <button type=\"reset\" class=\"btn btn-primary\">Annuler</button>
</form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "membre/createMembre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Créer un membre{% endblock %}

{% block body %}
<div class=\"example-wrapper\">
    <h1>Créer un Membre</h1>
</div>
<form action=\"/insertMembre\" method=\"post\">
  <fieldset>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Nom du nouveau membre</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"nomMembre\" placeholder=\"Entrer un nom\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Prénom du nouveau membre</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"prenomMembre\" placeholder=\"Entrer un prenom\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Email du nouveau membre</label>
      <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"emailMembre\" placeholder=\"Entrer un email\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Password du nouveau membre</label>
      <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"passwordMembre\" placeholder=\"Entrer un email\">
      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Rôle du nouveau membre</label>
      <select class=\"form-control\" name=\"roleMembre\">
\t\t\t<option  class=\"form-control\" value=\"3\"> Rédacteur </option>
\t\t\t<option  class=\"form-control\" value=\"2\"> Technicien </option>
\t\t\t<option  class=\"form-control\" value=\"1\"> Administrateur </option>
\t  </select>
    </div>
  </fieldset>
   <button type=\"submit\" class=\"btn btn-primary\">Créer</button>
   <button type=\"reset\" class=\"btn btn-primary\">Annuler</button>
</form>
{% endblock %}
", "membre/createMembre.html.twig", "/var/www/html/ticket-iut/templates/membre/createMembre.html.twig");
    }
}
