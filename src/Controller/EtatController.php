<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Etat;

class EtatController extends AbstractController
{
    /**
     * @Route("/etat", name="etat")
     */
    public function index(Request $request)
    {
		$sess = $request->getSession();
        return $this->render('etat/index.html.twig', [
            'controller_name' => 'EtatController',
        ]);
    }
	/**
     * @Route("/createEtat", name="create_etat")
     */
    public function createEtat(Request $request, EntityManagerInterface $manager)
    {
		$etat = new Etat();
		$nomEtat = $request->request->get("nomEtat");
		$etat->setNom($nomEtat);
		$manager->persist($etat);
        $manager->flush();        
		return $this->redirectToRoute('liste_etat');
		
    }
	/**
     * @Route("/listeEtat", name="liste_etat")
     */
    public function listeEtat(Request $request, EntityManagerInterface $manager)
    {
		$listeEtat = $manager->getRepository(Etat::class)->findAll();
        return $this->render('etat/liste.html.twig', [
            'listeEtat' => $listeEtat,
        ]);
    }
}
