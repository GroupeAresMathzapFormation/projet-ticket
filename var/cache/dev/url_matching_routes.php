<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/etat' => [[['_route' => 'etat', '_controller' => 'App\\Controller\\EtatController::index'], null, null, null, false, false, null]],
        '/createEtat' => [[['_route' => 'create_etat', '_controller' => 'App\\Controller\\EtatController::createEtat'], null, null, null, false, false, null]],
        '/listeEtat' => [[['_route' => 'liste_etat', '_controller' => 'App\\Controller\\EtatController::listeEtat'], null, null, null, false, false, null]],
        '/createMembre' => [[['_route' => 'create_membre', '_controller' => 'App\\Controller\\MembreController::createMembre'], null, null, null, false, false, null]],
        '/insertMembre' => [[['_route' => 'insert_membre', '_controller' => 'App\\Controller\\MembreController::insertMembre'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'login', '_controller' => 'App\\Controller\\SecurityController::index'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'connexion', '_controller' => 'App\\Controller\\SecurityController::connexion'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/ticket' => [[['_route' => 'ticket', '_controller' => 'App\\Controller\\TicketController::ticket'], null, null, null, false, false, null]],
        '/listeTicket' => [[['_route' => 'liste_ticket', '_controller' => 'App\\Controller\\TicketController::listeTicket'], null, null, null, false, false, null]],
        '/insertTicket' => [[['_route' => 'insert_ticket', '_controller' => 'App\\Controller\\TicketController::insertTicket'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], null, null, null, true, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/delTicket/([^/]++)(*:61)'
                .'|/api(?'
                    .'|(?:/(index)(?:\\.([^/]++))?)?(*:103)'
                    .'|/(?'
                        .'|docs(?:\\.([^/]++))?(*:134)'
                        .'|contexts/(.+)(?:\\.([^/]++))?(*:170)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        61 => [[['_route' => 'del_ticket', '_controller' => 'App\\Controller\\TicketController::delTicket'], ['id'], null, null, false, true, null]],
        103 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['index', '_format'], null, null, false, true, null]],
        134 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_format'], null, null, false, true, null]],
        170 => [
            [['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['shortName', '_format'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
