<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ticket/index.html.twig */
class __TwigTemplate_ee2275e5f26cb5d410847a82efa58246285db0b8fd0b2871944f7e00c1880b13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "ticket/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "ticket/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Créer un ticket";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"example-wrapper\">
    <h1>Créer un Ticket</h1>
</div>
<form action=\"/insertTicket\" method=\"post\">
  <fieldset>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Description</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"description\" placeholder=\"Entrer une description\">
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Technicien</label>
      <select name=\"technicien\" class=\"form-control\" id=\"exampleInputEmail1\">
\t\t";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["membres"]) || array_key_exists("membres", $context) ? $context["membres"] : (function () { throw new RuntimeError('Variable "membres" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 19
            echo "\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 19), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "nom", [], "any", false, false, false, 19), "html", null, true);
            echo "  </option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "\t  </select>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Etat</label>
      <select name=\"etat\" class=\"form-control\" id=\"exampleInputEmail1\">
\t\t";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["etats"]) || array_key_exists("etats", $context) ? $context["etats"] : (function () { throw new RuntimeError('Variable "etats" does not exist.', 26, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 27
            echo "\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 27), "html", null, true);
            echo "\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "nom", [], "any", false, false, false, 27), "html", null, true);
            echo "  </option>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "\t  </select>
    </div>
\t<div class=\"form-group\">
\t\t<label for=\"dateMax\">Date Max :</label>
\t\t<input type=\"date\" class=\"form-control\" id=\"dateMax\" name=\"dateMax\">
\t</div>

  </fieldset>
   <button type=\"submit\" class=\"btn btn-primary\">Créer</button>
   <button type=\"reset\" class=\"btn btn-primary\">Annuler</button>
</form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "ticket/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 29,  113 => 27,  109 => 26,  102 => 21,  91 => 19,  87 => 18,  73 => 6,  66 => 5,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Créer un ticket{% endblock %}

{% block body %}
<div class=\"example-wrapper\">
    <h1>Créer un Ticket</h1>
</div>
<form action=\"/insertTicket\" method=\"post\">
  <fieldset>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Description</label>
      <input type=\"text\" class=\"form-control\" id=\"exampleInputEmail1\" name=\"description\" placeholder=\"Entrer une description\">
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Technicien</label>
      <select name=\"technicien\" class=\"form-control\" id=\"exampleInputEmail1\">
\t\t{% for item in membres %}
\t\t\t<option value=\"{{ item.id }}\"> {{ item.nom }}  </option>
\t\t{%endfor %}
\t  </select>
    </div>
\t<div class=\"form-group\">
      <label for=\"exampleInputEmail1\">Etat</label>
      <select name=\"etat\" class=\"form-control\" id=\"exampleInputEmail1\">
\t\t{% for item in etats %}
\t\t\t<option value=\"{{ item.id }}\"> {{ item.nom }}  </option>
\t\t{%endfor %}
\t  </select>
    </div>
\t<div class=\"form-group\">
\t\t<label for=\"dateMax\">Date Max :</label>
\t\t<input type=\"date\" class=\"form-control\" id=\"dateMax\" name=\"dateMax\">
\t</div>

  </fieldset>
   <button type=\"submit\" class=\"btn btn-primary\">Créer</button>
   <button type=\"reset\" class=\"btn btn-primary\">Annuler</button>
</form>
{% endblock %}
", "ticket/index.html.twig", "/var/www/html/ticket-iut/templates/ticket/index.html.twig");
    }
}
