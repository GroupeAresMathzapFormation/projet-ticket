<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository; 
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Etat;
use App\Entity\Membre;
use App\Entity\Ticket;

class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="ticket")
     */
    public function ticket(Request $request, EntityManagerInterface $manager)
    {
		
		//on récupère tous les membres.
		$membres = $manager->getRepository(Membre::class)->findAll();
		$etats = $manager->getRepository(Etat::class)->findAll();
        return $this->render('ticket/index.html.twig', 
		[
            'membres' => $membres,
            'etats' => $etats,
        ]);
    }
	/**
     * @Route("/delTicket/{id}", name="del_ticket")
     */
    public function delTicket(Ticket $ticket,Request $request, EntityManagerInterface $manager)
    {
		$manager->remove($ticket);
		$manager->flush();
		return $this->redirectToRoute('liste_ticket');
	}
	/**
     * @Route("/listeTicket", name="liste_ticket")
     */
    public function listeTicket(Request $request, EntityManagerInterface $manager)
    {
		
		//on récupère tous les membres.
		$tickets = $manager->getRepository(Ticket::class)->findAll();
        return $this->render('ticket/liste.html.twig', 
		[
            'tickets' => $tickets
        ]);
    }
	/**
     * @Route("/insertTicket", name="insert_ticket")
     */
    public function insertTicket(Request $request, EntityManagerInterface $manager)
    {
		//on démarre la session
		$sess = $request->getSession();
		//on récupère le membre en cours (qui a créé le ticket)
		$membre = $manager->getRepository(Membre::class)->findOneById($sess->get('idUtilisateur'));
		//on insert dans la table
		$ticket = new Ticket();
		$ticket->setDescription($request->request->get("description"));
		$ticket->setActif(1);
		$ticket->setCreatedAt(new \Datetime());
		$ticket->setDateMax(new \Datetime($request->request->get("dateMax")));
		$ticket->setMembre($membre);
		$ticket->setTechnicien($manager->getRepository(Membre::class)->findOneById($request->request->get("technicien")));
		$ticket->setEtat($manager->getRepository(Etat::class)->findOneById($request->request->get("etat")));
		$manager->persist($ticket);
		$manager->flush();
		return $this->redirectToRoute('liste_ticket');
	}	
	
}
